import React, {Component} from "react";
import MovieSingle from "../../Components/MovieSingle/MovieSingleContainer";

export default class extends Component {

    render(){
        const movieId = this.props.match.params.number;
        return(
            <div>
                <MovieSingle movieId={movieId}/>
            </div>
        )
    }
}