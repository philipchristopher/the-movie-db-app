import React, {Component} from "react";
import Logo from "./Assets/the-movie-db-logo.svg";
import ImageBg from "./Assets/app-background-3.png";

import MoviesSearch from "../../Components/MoviesSearch/MoviesSearchContainer";
import MoviesAll from "../../Components/MoviesAll/MoviesAllContainer";
import "./PageHome.scss"

export default class extends Component {

    render(){
        return(
            <div> 
                <div className="home-header" style={{backgroundImage: "url(" + ImageBg + ")" }}>
                    <img src={Logo} style={{width: "70px"}} className="img-fluid" alt="the movie db logo" />
                </div>
                <MoviesSearch />
                <h3 className="mb-3">Popular Movies</h3>
                <MoviesAll movieCategory="popular"/>
            </div>
        )
    }
}