import React, {Component} from "react";
import "./MovieSingleStyles.scss";
import {Link} from "react-router-dom";
import {ReactComponent as IconArrowLeft} from "../../Assets/arrow-left.svg";
import moment from "moment";

export default class extends Component {

    componentWillMount(){
        this.props.getSingleMovieAction(this.props.movieId);
    }

    componentWillUnmount(){
        this.props.removeMovieSingleAction();
    }

    render(){

        // show error message
        if(this.props.movieSingleError){
            return(
                <div className="alert alert-warning"><strong>Error</strong>: {this.props.movieSingleErrorMessage}</div>
            )
        }

        // show loading message
        if(this.props.movieSingleFetching){
            return(
                <div>Retrieving movie data</div>
            )
        }

        
        const movie = this.props.movieSingle;

        const getMovieBg = (movie.backdrop_path) 
            ? "url('https://image.tmdb.org/t/p/original/" + movie.backdrop_path + "')"
            : "url(https://placehold.it/500x750?text=No+Image)";
        
        const getMovieYear = moment(movie.release_date, "YYYY-MM-DD").format("YYYY");

        const getRuntimeInHoursAndMinutes = (runtime) => {
            let hours = (runtime / 60);
            let rhours = Math.floor(hours);
            let minutes = (hours - rhours) * 60;
            let rminutes = Math.round(minutes);
            return rhours + "h " + rminutes + "min";
        }

        return(
            <div>
                <header className="movie-header">
                    
                    <div className="movie-back">
                        <Link to="/">
                            <IconArrowLeft/>
                        </Link>
                    </div>
                    <div className="movie-cover-bg" style={{ backgroundImage: getMovieBg }}> </div>
                    <div className="row no-gutters flex-nowrap">
                        <div className="col-auto">
                            <img src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`} alt="movie screenshot" className="img-fluid movie-thumbnail"/>
                        </div>
                        <div className="col">
                            <div className="px-4 pt-4">
                                <h1 className="movie-title">{movie.title}</h1>                                
                                <div className="movie-date">{getMovieYear} &middot; {movie.vote_average * 10}&#37; User Score</div>
                                <div className="movie-runtime">{getRuntimeInHoursAndMinutes(movie.runtime)}</div>
                            </div>
                        </div>

                    </div>
                </header>
                <hr className="mb-4"/>
                
                <h2 className="movie-small-title">Overview</h2>
                <p>{movie.overview}</p>
            </div>
        )

        
    }
}