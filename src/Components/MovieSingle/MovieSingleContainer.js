import MovieSingle from "./MovieSingleComponent";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {getSingleMovieAction, removeMovieSingleAction} from "../../Actions/MovieSingleAction";

function mapStateToProps(state){
    return state.movieSingle;
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getSingleMovieAction,
        removeMovieSingleAction
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps, null)(MovieSingle);