import React, {Component} from "react";
import {Link} from "react-router-dom";
import {ReactComponent as IconSearch} from "../../Assets/search.svg";
import moment from "moment";
import "./MoviesSearchStyles.scss";

export default class extends Component {

    constructor(props){
        super(props);

        this.textInputRef = React.createRef();
        this.formSubmit = this.formSubmit.bind(this);
        this.textUpdate = this.textUpdate.bind(this);
        this.getVoteColor = this.getVoteColor.bind(this);
    }

    formSubmit(event){
        event.preventDefault();

        const searchQuery = event.target.elements.searchQuery.value;
        if(searchQuery.trim() !== ""){
            this.props.searchMoviesAllAction(searchQuery);
        }
        this.textInputRef.current.focus();
    }

    getVoteColor(vote){
        if(vote < 33){
            return "pink";
        } else if (vote < 66){
            return "purple";
        } else {
            return "green";
        }
    }

    textUpdate(e){
        if(e.target.value === ""){
            this.props.searchMoviesClearAction();
        }
    }

    render(){

        const getMovieMonthAndYear = (date) => moment(date, "YYYY-MM-DD").format("MMMM YYYY");

        const searchResults = () => {

            if(!this.props.moviesHasSearched){
                return;
            }

            if(this.props.moviesSearchingError){
                return(
                    <div className="alert alert-warning"><strong>Error:</strong> {this.props.moviesSearchingErrorMessage}</div>
                )
            }    

            if(this.props.moviesSearching){
				return(
					<div className="text-center">Searching Movies!</div>
				)
            }

            if(this.props.moviesSearched.length > 0){

                const moviesSearched = this.props.moviesSearched.map( movie => 
                    <div key={movie.id} className="movie col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
                        <Link to={`/movie/${movie.id}`}>
                            <div className="position-relative mb-2">
                                <div className={`rating-badge bg-${this.getVoteColor(movie.vote_average*10)}`}>
                                    {movie.vote_average * 10 }&#37;
                                </div>
                                {
                                    movie.poster_path !== null ?
                                    <img src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`} alt="movie screenshot" className="img-fluid movie-grid-thumbnail"/>
                                    :
                                    <img src="http://placehold.it/500x750?text=No+Image" alt="placeholder" className="img-fluid movie-grid-thumbnail"/>
                                }
                            </div>
                        </Link>
                        <div style={{fontSize: ".875em"}}>
                            <div className="movie-smaller-title text-white">{movie.title}</div>
                            <div className="movie-date">{getMovieMonthAndYear(movie.release_date)}</div>
                        </div>
                    </div>
                )

                return (
                    <section className="mb-5">
                        <div className="row">
                            { moviesSearched }
                        </div>
                    </section>
                )

            } else {
                return(
                    <div className="text-center"><strong>No</strong> movies found.</div>
                )
            }
            
            
        }

        return(
            <div className="mb-5">
                <form onSubmit={this.formSubmit} autoComplete="off" noValidate className="mb-5">
                    <div className="position-relative">
                        <input type="search"
                                id="searchQuery"
                                className="form-control txt-search"
                                placeholder="Search"
                                onChange={this.textUpdate}
                                ref={this.textInputRef} />
                        <button className="btn-search"><IconSearch/></button>
                    </div>
                </form>

                { searchResults() }
            </div>
        )
    }
}