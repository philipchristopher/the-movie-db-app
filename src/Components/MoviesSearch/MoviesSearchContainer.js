import MoviesSearch from "./MoviesSearchComponent";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {searchMoviesAllAction, searchMoviesClearAction} from "../../Actions/MoviesSearchAction";

function mapStateToProps(state){
    return state.moviesSearch;
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        searchMoviesAllAction, searchMoviesClearAction
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps, null)(MoviesSearch);