import React, {Component} from "react";
import {Link} from "react-router-dom";
import moment from "moment";
import "./MoviesAllStyles.scss";

export default class extends Component {

    componentWillMount(){
        this.props.getMoviesAllAction(this.props.movieCategory);
    }

    render(){

        // Show error message
        if(this.props.moviesAllError){
            return(
                <div className="alert alert-warning"><strong>Error:</strong> {this.props.moviesAllErrorMessage}</div>
            )
        }

        // Show loading message
        if(this.props.moviesAllFetching){
            return(
                <div>Loading movies</div>
            )
        }

        const getVoteColor = (vote) => {
            if(vote < 33){
                return "pink";
            } else if (vote < 66){
                return "purple";
            } else {
                return "green";
            }
        }

        const getMovieMonthAndYear = (date) => moment(date, "YYYY-MM-DD").format("MMMM YYYY");

        return(
            <div className="row">
                { this.props.moviesAll.map( movie => 
                    <div key={movie.id} className="movie col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
                        <Link to={`/movie/${movie.id}`}>
                            <div className="position-relative mb-2">
                                <div className={`rating-badge bg-${getVoteColor(movie.vote_average*10)}`}>
                                    {movie.vote_average * 10 }&#37;
                                </div>
                                {
                                    movie.poster_path !== null ?
                                    <img src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`} alt="movie screenshot" className="img-fluid movie-grid-thumbnail"/>
                                    :
                                    <img src="http://placehold.it/500x750?text=No+Image" alt="placeholder" className="img-fluid movie-grid-thumbnail"/>
                                }
                            </div>
                        </Link>
                        <div style={{fontSize: ".875em"}}>
                            <div className="movie-smaller-title text-white">{movie.title}</div>
                            <div className="movie-date">{getMovieMonthAndYear(movie.release_date)}</div>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}