import MoviesAll from "./MoviesAllComponent";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {getMoviesAllAction} from "../../Actions/MoviesAllAction";

function mapStateToProps(state){
    return state.moviesAll;
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getMoviesAllAction
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps, null)(MoviesAll);