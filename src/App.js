import React, {Component} from 'react';
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import PageHome from "./Pages/Home/PageHome";
import PageMovieSingle from "./Pages/MovieSingle/PageMovieSingle";

export default class App extends Component {
	render (){
		return (
			<main className="d-flex flex-column h-100">
				<div className="container-fluid">
					<Router>
						<Switch>
							<Route exact path="/" component={PageHome}/>
							<Route path="/movie/:number" component={PageMovieSingle} />
						</Switch>
					</Router>
				</div>
				<footer className="foot-note small text-muted text-center mt-auto p-3 bg-black">Demo for Jumbo Interactive. Built by <a
                	href="http://www.philipchristopher.com/" rel="noopener noreferrer" target="_blank">Philip Christopher</a>. Thanks! &lt;3
                </footer>
			</main>
		);
	}
}