const initialState = {
    movieSingleFetching: true,
    movieSingle: [],
    movieSingleError: false,
    movieSingleErrorMessage: ""
}

export default function( state = initialState, action){
    switch(action.type){
        case "FETCH_MOVIE_SINGLE_SUCCESS":
            return(
                Object.assign({}, state, {
                    movieSingleFetching: false,
                    movieSingle: action.data
                })
            )
        case "FETCH_MOVIE_SINGLE_FAILURE":
            return(
                Object.assign({}, state, {
                    movieSingleFetching: false,
                    movieSingleError: true,
                    movieSingleErrorMessage: action.message
                })
            )
        case "REMOVE_MOVIE_SINGLE":
            return(
                Object.assign({}, state, {
                    movieSingleFetching: true,
                    movieSingle: [],
                    movieSingleError: false,
                    movieSingleErrorMessage: ""
                })
            )
        default:
            return state;
    }
}