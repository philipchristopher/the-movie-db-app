const initialState = {
    moviesSearching: false,
    moviesSearched: [],
    moviesHasSearched: false,
    moviesSearchingError: false,
    moviesSearchingErrorMessage: ""
}

export default function( state = initialState, action){
    switch( action.type){
        case "SEARCH_MOVIES_ALL_SUCCESS":
            return(
                Object.assign({}, state, {
                    moviesSearching: false,
                    moviesSearched: action.data
                })
            );
        case "SEARCH_MOVIES_ALL_FAILURE":
            return(
                Object.assign({}, state, {
                    moviesSearching: false,
                    moviesSearchingError: true,
                    moviesSearchingErrorMessage: action.message
                })
            )
        case "SEARCHING_MOVIES":
            return(
                Object.assign({}, state, {
                    moviesSearching: true,
                    moviesHasSearched: true
                })
            )
        case "SEARCH_MOVIES_CLEAR":
            return(
                Object.assign({}, state, {
                    moviesSearching: false,
                    moviesHasSearched: false,
                    moviesSearched: [],
                    moviesSearchingError: false,
                    moviesSearchingErrorMessage: ""
                })
            )
        default:
            return state;
    }
}