const initialState = {
    moviesAllFetching: true,
    moviesAll: [],
    moviesAllError: false,
    moviesAllErrorMessage: ""
}

export default function( state = initialState, action){
    switch( action.type){
        case "FETCH_MOVIES_ALL_SUCCESS":
            return(
                Object.assign({}, state, {
                    moviesAllFetching: false,
                    moviesAll: action.data
                })
            );
        case "FETCH_MOVIES_ALL_FAILURE":
            return(
                Object.assign({}, state, {
                    moviesAllFetching: false,
                    moviesAllError: true,
                    moviesAllErrorMessage: action.message
                })
            );
        default:
            return state;
    }
}