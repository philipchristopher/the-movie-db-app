import {combineReducers} from "redux";

import MoviesAllReducer from "./MoviesAllReducer";
import MoviesSearch from "./MoviesSearchReducer";
import MovieSingle from "./MovieSingleReducer";

const allReducers = combineReducers({
    moviesAll: MoviesAllReducer,
    moviesSearch: MoviesSearch,
    movieSingle: MovieSingle
});

export default allReducers;