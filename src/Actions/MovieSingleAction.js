import * as api from "../Endpoints/api";

export const getSingleMovieAction = (movieId) => (dispatch) => {
    return api.fetchMovieSingle(movieId).then(
        success => {
            dispatch({
                type: "FETCH_MOVIE_SINGLE_SUCCESS",
                data: success.data
            })
        },
        error => {
            dispatch({
                type: "FETCH_MOVIE_SINGLE_FAILURE",
                message: error.message
            })
        }
    )
};

export const removeMovieSingleAction = () => (dispatch) => {
    dispatch({
        type: "REMOVE_MOVIE_SINGLE"
    })
};