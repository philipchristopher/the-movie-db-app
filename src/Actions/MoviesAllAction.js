import * as api from "../Endpoints/api";

export const getMoviesAllAction = (category) => (dispatch) => {
    return api.fetchMoviesAll(category).then(
        success => {
            dispatch({
                type: "FETCH_MOVIES_ALL_SUCCESS",
                data: success.data.results
            })
        },
        error => {
            dispatch({
                type: "FETCH_MOVIES_ALL_FAILURE",
                message: error.message
            })
        }
    )
};