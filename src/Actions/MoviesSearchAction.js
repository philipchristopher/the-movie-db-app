import * as api from "../Endpoints/api";

export const searchMoviesAllAction = (query) => (dispatch) => {

    dispatch({
        type: "SEARCHING_MOVIES"
    });

    return api.searchMoviesAll(query).then(
        success => {
            dispatch({
                type: "SEARCH_MOVIES_ALL_SUCCESS",
                data: success.data.results
            })
        },
        error => {
            dispatch({
                type: "SEARCH_MOVIES_ALL_FAILURE",
                message: error.message
            })
        }
    )
};

export const searchMoviesClearAction = (query) => (dispatch) => {
    dispatch({
        type: "SEARCH_MOVIES_CLEAR"
    })
};