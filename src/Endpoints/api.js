import axios from "axios";

const movieURL = "https://api.themoviedb.org/3/";
const movieKey = "?api_key=6ed12e064b90ae1290fa326ce9e790ff";

export const fetchMoviesAll = (category) =>
    axios.get(movieURL + "movie/" + category + movieKey).then( response => response );

export const searchMoviesAll = (query) =>
    axios.get(movieURL + "search/movie" + movieKey + "&query=" + query).then( response => response);

export const fetchMovieSingle = (movieId) =>
    axios.get(movieURL + "movie/" + movieId + movieKey).then( response => response );